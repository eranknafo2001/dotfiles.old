#!/bin/bash

mkdir -p $HOME/.config
mkdir -p $HOME/Pictures
mkdir -p $HOME/.local/bin
mkdir -p $HOME/.ssh

for item in bin/*; do
    ln -sf $(realpath $item) -T $HOME/.local/$item
done

for item in ssh/* config/* emacs.d; do
    ln -sf $(realpath $item) -T $HOME/.$item
done

ln -sf $(realpath git/gitconfig) -T $HOME/.gitconfig

ln -sf $(realpath config/nvim/init.vim) -T $HOME/.vimrc
ln -sf $(realpath config/nvim) -T $HOME/.vim

ln -sf $(realpath zsh/zshrc) -T $HOME/.zshrc
ln -sf $(realpath bash/bashrc) -T $HOME/.bashrc

[ -x "$(command -v pacman)" ] && [[ $1 == "install" ]] && sudo pacman --needed -Sy \
    picom variety alacritty qtile neovim nodejs \
    npm yarn udiskie feh ttf-ubuntu-font-family \
    rofi xdg-utils zsh exa bat fzf slock zsh    \
    lightdm light-locker pamixer fish dunst

[[ $1 == "install" ]] && ( [ -x "$(command -v starship)" ] || curl -fsSL https://starship.rs/install.sh | bash )

shell=$(awk -F: -v user="$USER" '$1 == user {print $NF}' /etc/passwd)
[[ $shell == *"zsh"* ]] && [ -x "$(command -v zsh)" ] || chsh -s $(which zsh)
