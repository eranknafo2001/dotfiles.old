from __future__ import annotations
import subprocess
from typing import Optional
from subprocess import CalledProcessError
from libqtile.widget import base


class KeyboardLayout(base.ThreadPoolText):
    """Displays memory/swap usage

    layout: Returns amount of swap in use
    variant: Returns swap in use as a percentage


    Widget requirements: xkb-switch installed on the system.
    """

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("format", "{layout} {variant}", "Formatting for field names."),
        ("update_interval", 1.0, "Update interval for the Memory"),
    ]

    def __init__(self, **config):
        super().__init__("", **config)
        self.add_defaults(KeyboardLayout.defaults)

    def poll(self):
        try:
            setxkbmap_out = subprocess.check_output(["xkb-switch"])
            keyboard = _Keyboard.from_name(setxkbmap_out.decode().strip())
            val = {}
            val["layout"] = keyboard.layout
            val["variant"] = keyboard.variant if keyboard.variant else ""
            return self.format.format(**val)
        except CalledProcessError as e:
            self.log.error(f"Can not change the keyboard layout: {e}")
        except OSError as e:
            self.log.error(f"Please, check that setxkbmap is available {e}")
        return "unknown"


class _Keyboard(object):
    """
    Canonical representation of a keyboard layout. It provides some utility
    methods to build/transform it from/to some other representations.
    """

    def __init__(self, layout: str, variant: Optional[str] = None):
        """Accept a dict containing as keys the layout and variant of a keyboard layout."""
        self.layout = layout
        self.variant = variant

    def __str__(self) -> str:
        if not self.variant:
            return self.layout
        else:
            return self.layout + " " + self.variant

    @staticmethod
    def from_name(name: str) -> _Keyboard:
        """
        Accept a setxkbmap query represented as a string.
        """
        name = name.strip()
        pos = name.find("(")
        if pos == -1:
            return _Keyboard(name)
        test = _Keyboard(name[:pos].strip(), name[pos + 1 : -2].strip())
        return test
