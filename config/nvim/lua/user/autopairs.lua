local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "windwp/nvim-autopairs",
  config = function()
    local autopairs = safe_require("nvim-autopairs")
    if not autopairs then
      return
    end
    autopairs.setup({})
  end,
})
