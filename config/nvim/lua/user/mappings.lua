local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "eranknafo2001/mapx.nvim",
  as = "mapx",
  config = function()
    local m = safe_require("mapx")
    if not m then
      return
    end
    m.setup({ whichkey = true })

    m.group({ prefix = "<leader>" }, function()
      m.nnoremap("p", '"+p', "Clipbord Paste")
      m.vnoremap("p", '"+p', "Clipbord Paste")
      m.nnoremap("P", '"+P', "Clipbord Paste")
      m.nnoremap("y", '"+y', "Clipbord Yank")
      m.vnoremap("y", '"+y', "Clipbord Yank")
    end)

    m.tnoremap("<C-j><C-j>", "<C-\\><C-N>", "silent")
  end,
})
