local M = {}

function _G.safe_require(module_name, silent)
  silent = silent or false
  local status, module = pcall(require, module_name)
  if not status then
    if not silent then
      vim.notify(module_name .. " wasnt found")
    end
    return status
  end
  return module
end

function M.Set(list)
  local set = {}
  for _, l in ipairs(list) do
    set[l] = true
  end
  return set
end

return M
