local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "b3nj5m1n/kommentary",
  after = "mapx",
  config = function()
    local m = safe_require("mapx")
    if not m then
      return
    end

    m.nmap("gcc", "<Plug>kommentary_line_default")
    m.nmap("gc", "<Plug>kommentary_motion_default")
    m.vmap("gc", "<Plug>kommentary_visual_default<C-c>")
    local kommentary_config = safe_require("kommentary.config")
    if not kommentary_config then
      return
    end

    kommentary_config.configure_language("default", {
      prefer_single_line_comments = true,
    })
  end,
})
