local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "nvim-orgmode/orgmode",
  config = function()
    local orgmode = safe_require("orgmode")
    if not orgmode then
      return
    end
    orgmode.setup({})
  end,
})

use({
  "akinsho/org-bullets.nvim",
  config = function()
    local org_bullets = safe_require("org-bullets")
    if not org_bullets then
      return
    end
    org_bullets.setup({
      symbols = { "◉", "○", "✸", "✿" },
    })
  end,
})
