local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "dstein64/vim-startuptime",
  cmd = "StartupTime",
  config = function()
    vim.g.startuptime_tries = 10

    local m = safe_require("mapx")
    if not m then
      return
    end

    m.nnoremap("q", "<cmd>q<CR>", { ft = "startuptime" })
  end,
})
