local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
  vim.cmd([[packadd packer.nvim]])
end

local packer = safe_require("packer")
if not packer then
  return nil
end

packer.init({})

local use = packer.use
packer.reset()

use({ "wbthomason/packer.nvim" })

return packer
