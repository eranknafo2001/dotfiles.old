local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "AckslD/nvim-neoclip.lua",
  requires = { { "tami5/sqlite.lua", module = "sqlite" } },
  after = "telescope",
  config = function()
    local neoclip = safe_require("neoclip")
    if not neoclip then
      return
    end
    neoclip.setup({
      history = 1000,
      enable_persistent_history = true,
      preview = true,
      default_register = '"',
      default_register_macros = "q",
      enable_macro_history = true,
      keys = {
        telescope = {
          i = {
            select = "<cr>",
            paste = "<c-p>",
            paste_behind = "<c-o>",
            replay = "<c-q>",
            custom = {},
          },
          n = {
            select = "<cr>",
            paste = "p",
            paste_behind = "P",
            replay = "q",
            custom = {},
          },
        },
      },
    })
    local telescope = safe_require("telescope")
    if telescope then
      telescope.load_extension("neoclip")

      local m = safe_require("mapx")
      if not m then
        return
      end
      m.nnoremap("<leader>sc", "<cmd>Telescope neoclip")
    end
  end,
})
