local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "lukas-reineke/indent-blankline.nvim",
  config = function()
    local indent = safe_require("indent_blankline")
    if not indent then
      return
    end
    indent.setup({
      show_current_context = true,
      show_current_context_start = false,
      filetype_exclude = {"startup"},
    })
  end,
})
