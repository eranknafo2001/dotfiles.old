local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "ThePrimeagen/harpoon",
  requires = { "nvim-lua/plenary.nvim" },
  after = { "telescope", "mapx" },
  config = function()
    local harpoon = safe_require("harpoon")
    if not harpoon then
      return
    end
    harpoon.setup({})
    local telescope = safe_require("telescope")
    local m = safe_require("mapx")
    if not m then
      return
    end
    if telescope then
      telescope.load_extension("harpoon")
      m.nnoremap("<leader>sc", "<cmd>Telescope harpoon marks<CR>", "silent", "Harpoon Marks")
      m.nnoremap("<leader>xs", "<cmd>Telescope harpoon marks<CR>", "silent", "Harpoon Marks")
    end
    local harpoon_mark = safe_require("harpoon.mark")
    if not harpoon_mark then
      return
    end
    m.group({ prefix = "<leader>x" }, "Harpoon", function()
      m.nnoremap("a", function()
        harpoon_mark.add_file()
      end, "Mark current file")
      m.nnoremap("d", function()
        harpoon_mark.rm_file()
      end, "Remove mark current file")
      m.nnoremap("d", function()
        harpoon_mark.toggle_file()
      end, "Toggle mark current file")
      m.nnoremap("c", function()
        harpoon_mark.clear_all()
      end, "Clear all marks")
    end)
  end,
})
