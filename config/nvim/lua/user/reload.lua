local M = {}

function M.reload()
  local reload = safe_require("plenary.reload")
  if reload then
    reload.reload_module("user")
  end
  vim.cmd("source $MYVIMRC")
end

local m = safe_require("mapx")
if m then
  m.nnoremap("<leader>r", function()
    M.reload()
  end)
  m.cmdbang("ReloadConfig", function()
    M.reload()
  end)
end

return M
