local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "tpope/vim-surround",
  requires = "tpope/vim-repeat",
  after = "mapx",
  setup = function()
    vim.g.surround_no_mappings = true
  end,
  config = function()
    local m = safe_require("mapx")
    if not m then
      return
    end
    m.vnoremap("s", "S")
    m.nmap("ds", "<Plug>Dsurround")
    m.nmap("cs", "<Plug>Csurround")
    m.nmap("cS", "<Plug>CSurround")
    m.nmap("ys", "<Plug>Ysurround")
    m.nmap("yS", "<Plug>YSurround")
    m.nmap("yss", "<Plug>Yssurround")
    m.nmap("ySs", "<Plug>YSsurround")
    m.nmap("ySS", "<Plug>YSsurround")
    m.xmap("s", "<Plug>VSurround")
    m.xmap("gs", "<Plug>VgSurround")
  end,
})
