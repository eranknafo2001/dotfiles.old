local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "mbbill/undotree",
  after = "mapx",
  keys = { { "n", "<F4>" } },
  cmd = "UndotreeToggle",
  config = function()
    vim.g.undotree_SetFocusWhenToggle = 1
    local m = safe_require("mapx")
    if not m then
      return
    end

    m.nnoremap("<F4>", "<cmd>UndotreeToggle<CR>", "silent")
  end,
})
