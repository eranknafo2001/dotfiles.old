local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "kyazdani42/nvim-tree.lua",
  requires = { "kyazdani42/nvim-web-devicons" },
  after = "mapx",
  config = function()
    local nvim_tree = safe_require("nvim-tree")
    if not nvim_tree then
      return
    end
    nvim_tree.setup({})

    local m = safe_require("mapx")
    if m then
      m.nnoremap("<leader>-", "<cmd>NvimTreeFindFile<CR><cmd>NvimTreeOpen<CR>", "Open Explorer")
      m.nnoremap("<leader>-", "<cmd>NvimTreeClose<CR>", { ft = "NvimTree" }, "Close Explorer")
    end
  end,
})
