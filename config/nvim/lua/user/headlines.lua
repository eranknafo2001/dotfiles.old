local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "lukas-reineke/headlines.nvim",
  ft = { "markdown", "rmd", "vimwiki", "orgmode" },
  config = function()
    local headlines = safe_require("headlines")
    if not headlines then
      return
    end
    headlines.setup()
  end,
})
