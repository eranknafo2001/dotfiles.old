local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "hkupty/iron.nvim",
  cmd = {
    "IronRepl",
    "IronReplHere",
    "IronRestart",
    "IronSend",
    "IronFocus",
    "IronWatchCurrentFile",
    "IronUnwatchCurrentFile",
  },
  config = function()
    local iron = safe_require("iron")
    if not iron then
      return
    end

    iron.core.set_config({
      preferred = {
        python = "ipython",
      },
    })
  end,
})
