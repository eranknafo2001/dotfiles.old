local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "sindrets/diffview.nvim",
  requires = "nvim-lua/plenary.nvim",
  cmd = "DiffviewOpen",
  module = "diffview",
  config = function()
    local diffview = safe_require("diffview")
    if not diffview then
      return
    end
    diffview.setup()
  end,
})
