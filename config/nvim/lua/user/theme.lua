local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "sainnhe/gruvbox-material",
  config = function()
    vim.o.background = "dark"
    vim.cmd("colorscheme gruvbox-material")
  end,
})
