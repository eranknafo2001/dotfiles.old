local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "ahmedkhalf/project.nvim",
  after = "telescope",
  config = function()
    local project_nvim = safe_require("project_nvim")
    if not project_nvim then
      return
    end
    project_nvim.setup({})
    local telescope = safe_require("telescope")
    if not telescope then
      return
    end
    telescope.load_extension("projects")
    local m = safe_require("mapx")
    if not m then return end
    m.nnoremap("<leader>sp", "<cmd>Telescope project<CR>", "silent")
  end,
})
