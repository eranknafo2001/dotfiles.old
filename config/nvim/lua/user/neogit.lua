local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

local m = safe_require("mapx")
if m then
  m.nnoremap("<leader>g", "<cmd>Neogit<CR>", "Neogit", "silent")
end

use({
  "TimUntersberger/neogit",
  requires = "nvim-lua/plenary.nvim",
  after = "mapx",
  cmd = "Neogit",
  config = function()
    local neogit = safe_require("neogit")
    if not neogit then
      return
    end

    neogit.setup({
      integrations = {
        diffview = true,
      },
    })
  end,
})
