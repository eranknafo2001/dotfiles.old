local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "SmiteshP/nvim-gps",
  after = { "treesitter" },
  as = "gps",
  config = function()
    local gps = safe_require("nvim-gps")
    if not gps then
      return
    end
    gps.setup()
  end,
})
