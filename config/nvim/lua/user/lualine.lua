local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "nvim-lualine/lualine.nvim",
  as = "lualine",
  after = "gps",
  requires = { "kyazdani42/nvim-web-devicons", opt = true },
  config = function()
    local lualine = safe_require("lualine")
    if not lualine then
      return
    end
    local section_c = { { "filename", path = 1 } }
    local gps = safe_require("nvim-gps")
    if gps then
      table.insert(section_c, { gps.get_location, cond = gps.is_available })
    end
    lualine.setup({
      sections = {
        lualine_c = section_c,
      },
    })
  end,
})
