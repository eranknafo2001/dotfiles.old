local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "norcalli/nvim-terminal.lua",
  ft = "terminal",
  config = function()
    local terminal = safe_require("terminal")
    if not terminal then
      return
    end
    terminal.setup()
  end,
})
